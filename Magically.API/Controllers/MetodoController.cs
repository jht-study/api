﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Magically.API.Filters;
using Magically.Domain;
using Magically.Service.Interface;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Magically.API.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowAllHeaders")]
    [GetClaimsFilter]
    public class MetodoController : Controller
    {
        private readonly IMetodoService _service;
        private Expression<Func<Metodo, object>> _defaultOrder = x => x.TipoMetodo;

        public MetodoController(IMetodoService metodoService)
        {
            _service = metodoService;
        }

        protected void SetDefault(Expression<Func<Metodo, object>> defaultOrder)
        {
            _defaultOrder = defaultOrder;
        }

        [HttpGet()]
        public IActionResult Get()
        {
            var metodos = _service.Get<object>(_defaultOrder);
            return metodos != null ? (IActionResult)Ok(metodos) : NotFound();
        }
    }
}