﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Magically.API.Filters;
using Magically.Domain;
using Magically.Infra.Identity.Auth;
using Magically.Service.Interface;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Magically.API.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowAllHeaders")]
    [GetClaimsFilter]
    public class VisitaAcompanhadaController : ControllerBase
    {
        private readonly IVisitaAcompanhadaService _service;
        private Expression<Func<VisitaAcompanhada, object>> _defaultOrder = x => x.DescAcompanhada;

        public VisitaAcompanhadaController(IVisitaAcompanhadaService visitaAcompanhadaService)
        {
            _service = visitaAcompanhadaService;
        }

        protected void SetDefault(Expression<Func<VisitaAcompanhada, object>> defaultOrder)
        {
            _defaultOrder = defaultOrder;
        }

        [HttpGet()]
        public IActionResult Get()
        {
            var visitaAcompanhadas = _service.Get<object>(_defaultOrder);
            return visitaAcompanhadas != null ? (IActionResult)Ok(visitaAcompanhadas) : NotFound();
        }
    }
}