﻿using AutoMapper;
using Magically.API.AutoMapper;

namespace Magically.API.Configuration
{
    public static class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<ViewModelToDomainMappingProfile>();
            });
        }
    }
}
