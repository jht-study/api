﻿using Magically.Infra.Data.Repository.Concret;
using Magically.Infra.Data.Repository.Interface;
using Magically.Infra.Identity;
using Magically.Infra.Identity.Auth;
using Magically.Service.Concret;
using Magically.Service.Interface;
using Microsoft.Extensions.DependencyInjection;

namespace Magically.API.Configuration
{
    public static class ServiceRepositoryCollectionExtensions
    {
        public static IServiceCollection RegisterRepositoryServices(this IServiceCollection services)
        {
            //services
            services.AddScoped<IVisitaAcompanhadaService, VisitaAcompanhadaService>();
            services.AddScoped<IMetodoService, MetodoService>();

            //repositories
            services.AddScoped<IVisitaAcompanhadaRepository, VisitaAcompanhadaRepository>();
            services.AddScoped<IMetodoRepository, MetodoRepository>();

            //Auth
            services.AddScoped<IApplicationSignInManager, ApplicationSignInManager>();

            return services;
        }

    }
}
