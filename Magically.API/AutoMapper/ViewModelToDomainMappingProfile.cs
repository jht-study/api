﻿using AutoMapper;
using Magically.Domain;
using Magically.API.ViewModels;

namespace Magically.API.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile() : this("Profile") { }

        protected ViewModelToDomainMappingProfile(string profileName) : base(profileName)
        {
            CreateMap<LoginUserVM, USR_Usuarios>()
                 .ForPath(dest => dest.Setor, opt => opt.MapFrom(src => src.User))
                 .ForPath(dest => dest.Senha, opt => opt.MapFrom(src => src.Password));
        }
    }
}
