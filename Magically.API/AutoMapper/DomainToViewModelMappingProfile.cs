﻿using AutoMapper;
using Magically.Domain;
using Magically.API.ViewModels;

namespace Magically.API.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile() : this("Profile") { }

        protected DomainToViewModelMappingProfile(string profileName) : base(profileName)
        {
            CreateMap<USR_Usuarios, UserVM>();
        }
    }
}
