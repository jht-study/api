﻿using System.ComponentModel.DataAnnotations;

namespace Magically.API.ViewModels
{
    public class LoginUserVM
    {
        [Required]
        public string User { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
