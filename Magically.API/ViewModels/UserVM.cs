﻿namespace Magically.API.ViewModels
{
    public class UserVM
    {
        public int IDUsuario { get; set; }

        public string Nome { get; set; }

        public string Setor { get; set; }
    }
}
