﻿using Magically.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Magically.Domain
{
    public class USR_Usuarios
    {
        [Key]
        public int IDUsuario { get; set; }
        public string Nome { get; set; }
        public string Setor { get; set; }
        public string SetorCO { get; set; }
        public string SetorGD { get; set; }
        public string SetorGR { get; set; }
        public string SetorGN { get; set; }
        public string Senha { get; set; }
        public bool Ativo { get; set; }
        public Perfil Nivel { get; set; }
        public int IDEquipe { get; set; }
        public int IDEquipeGrupo { get; set; }
        public bool AceitaRevisita { get; set; }
        public string Filial { get; set; }
        public string DescricaoSetor { get; set; }
        public string Distribuidor { get; set; }
        public string Versao { get; set; }
        public bool Manutencao { get; set; }
        public int VisitaAvulsaPDV { get; set; }
        public int SegmentacaoRevisita { get; set; }
        public int Acesso { get; set; }
        public int DiasLimiteSincronizacao { get; set; }
        public int RezoneamentoNCiclo { get; set; }
        public int VisitaAvulsa { get; set; }
        public bool Overlap { get; set; }
        public bool OverlapAprovacao { get; set; }
        public int IDOverlapTipo { get; set; }
        public bool StatusRoteiro { get; set; }
        public int NCadMedico { get; set; }
        public int NCadHospital { get; set; }
        public int NCadPdv { get; set; }
        public bool VisitaMedWeb { get; set; }
        public bool VisitaHosWeb { get; set; }
        public bool VisitaPdvWeb { get; set; }
        public bool CadMedicoAprovacao { get; set; }
    }
}
