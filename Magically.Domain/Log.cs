﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Magically.Domain
{
    public class Log
    {
        [Key]
        public long IDLog { get; set; }
        public int IDUsuario { get; set; }
        public string EntityID { get; set; }
        public string EntityName { get; set; }
        public string Operation { get; set; }
        public DateTime LogDateTime { get; set; }
        public string ValuesChanges { get; set; }
    }
}
