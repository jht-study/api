﻿using Magically.Domain.Common;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Magically.Domain
{
    [Table("VisitaAcompanhada")]
    public class VisitaAcompanhada : BaseEntity
    {
        [Key]
        [JsonProperty("id_visita_acompanhada")]
        public int IDVisitaAcompanhada { get; set; }

        [JsonProperty("visita_acompanhada")]
        public string DescAcompanhada { get; set; }

        public string Sigla { get; set; }

        public bool Ativo { get; set; }
    }
}
