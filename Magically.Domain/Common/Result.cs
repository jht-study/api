﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Magically.Domain.Common
{
    public class Result<T> where T : class
    {
        public Result(T value)
        {
            Value = value;
        }

        /*public Result(T value) : this(null, value) { }

        public Result(ValidationResult validationResult, T value)
        {
            Messages = validationResult?.Errors.Select(x => x.ErrorMessage).ToList() ?? new List<string>();
            Value = value;
        }*/

        public T Value { get; set; }
        public IList<string> Messages { get; }
        public string SuccessMessage { get; set; }
        public bool Success { get { return Messages.Count == 0; } }
    }
}
