﻿using System.Collections.Generic;

namespace Magically.Domain.Common
{
    public class ResultList<T>
    {
        public IList<T> Items { get; set; }
    }
}
