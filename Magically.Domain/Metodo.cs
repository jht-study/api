﻿using Magically.Domain.Common;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Magically.Domain
{
    [Table("Metodo")]
    public class Metodo : BaseEntity
    {
        [Key]
        [JsonProperty("id_metodo")]
        public int IDMetodo { get; set; }

        public string TipoMetodo { get; set; }
    }
}
