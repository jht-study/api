﻿namespace Magically.Domain.Enums
{
    public enum Perfil
    {
        Representente = 1,
        Coodernador,
        GerenteDistrital,
        GerenteRegional,
        GerenteNacional,
        MKT
    }
}
