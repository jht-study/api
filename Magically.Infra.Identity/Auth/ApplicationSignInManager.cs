﻿using Magically.Domain;
using Magically.Infra.Identity.Configurations;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;

namespace Magically.Infra.Identity.Auth
{
    public class ApplicationSignInManager : IApplicationSignInManager
    {
        public object GenerateTokenAndSetIdentity(USR_Usuarios user, SigningConfigurations signingConfigurations, TokenConfigurations tokenConfigurations)
        {
            ClaimsIdentity identity = new ClaimsIdentity(
                    new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(JwtRegisteredClaimNames.UniqueName, user.IDUsuario.ToString()),
                        new Claim(ClaimTypes.Role, user.Nivel.ToString(), ClaimValueTypes.String, tokenConfigurations.Issuer)
                    }
                );

            DateTime creationDate = DateTime.Now;
            DateTime expireDate = creationDate + TimeSpan.FromSeconds(tokenConfigurations.Seconds);

            var handler = new JwtSecurityTokenHandler();
            var securityToken = handler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = tokenConfigurations.Issuer,
                Audience = tokenConfigurations.Audience,
                SigningCredentials = signingConfigurations.SigningCredentials,
                Subject = identity,
                NotBefore = creationDate,
                Expires = expireDate
            });
            var token = handler.WriteToken(securityToken);

            return new
            {
                authenticated = true,
                created = creationDate,
                expiration = expireDate,
                accessToken = token,
                name = user.Nome,
                id_usuario = user.IDUsuario,
                setor = user.Setor,
                nivel = user.Nivel.ToString(),
                message = "OK"
            };
        }
    }
}