﻿using Magically.Domain;
using Magically.Infra.Identity.Configurations;

namespace Magically.Infra.Identity.Auth
{
    public interface IApplicationSignInManager
    {
        object GenerateTokenAndSetIdentity(USR_Usuarios user, SigningConfigurations signingConfigurations, TokenConfigurations tokenConfigurations);
    }
}
