﻿using Magically.Domain;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Magically.Infra.Data.Mapping
{
    public class MetodoMap
    {
        public MetodoMap(EntityTypeBuilder<Metodo> entityBuilder)
        {
            entityBuilder.HasKey(t => t.IDMetodo);
            entityBuilder.Property(t => t.TipoMetodo);
        }
    }
}
