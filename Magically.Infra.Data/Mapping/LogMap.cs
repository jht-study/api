﻿using Magically.Domain;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Magically.Infra.Data.Mapping
{
    public class LogMap
    {
        public LogMap(EntityTypeBuilder<Log> entityBuilder)
        {
            entityBuilder.HasKey(t => t.IDLog);
            entityBuilder.Property(t => t.IDUsuario);
            entityBuilder.Property(t => t.EntityName);
            entityBuilder.Property(t => t.EntityID);
            entityBuilder.Property(t => t.Operation);
            entityBuilder.Property(t => t.LogDateTime);
            entityBuilder.Property(t => t.ValuesChanges);
        }
    }
}
