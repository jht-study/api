﻿using Magically.Domain;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Magically.Infra.Data.Mapping
{
    public class VisitaAcompanhadaMap
    {
        public VisitaAcompanhadaMap(EntityTypeBuilder<VisitaAcompanhada> entityBuilder)
        {
            entityBuilder.HasKey(t => t.IDVisitaAcompanhada);
        }
    }
}
