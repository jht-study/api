﻿using Magically.Domain;
using Magically.Domain.Common;
using Magically.Infra.Data.Repository.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Magically.Infra.Data.Repository.Interface
{
    public interface IMetodoRepository : IRepositoryGeneric<Metodo>
    {

    }
}
