﻿using Magically.Domain;
using Magically.Infra.Data.Repository.Generic;

namespace Magically.Infra.Data.Repository.Interface
{
    public interface IVisitaAcompanhadaRepository : IRepositoryGeneric<VisitaAcompanhada>
    {
    }
}
