﻿using Magically.Domain;
using Magically.Infra.Data.Repository.Generic;
using Magically.Infra.Data.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Magically.Infra.Data.Repository.Concret
{
    public class VisitaAcompanhadaRepository : RepositoryGeneric<VisitaAcompanhada>, IVisitaAcompanhadaRepository
    {
        public VisitaAcompanhadaRepository(ApplicationDbContext context) : base(context) { }
    }
}
