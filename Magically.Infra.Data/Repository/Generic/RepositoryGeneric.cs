﻿using Magically.Domain.Common;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Magically.Infra.Data.Repository.Generic
{
    public class RepositoryGeneric<TEntity> : IRepositoryGeneric<TEntity> where TEntity : class
    {
        protected readonly ApplicationDbContext _context;
        protected readonly DbSet<TEntity> _dbSet;

        public RepositoryGeneric(ApplicationDbContext context)
        {
            _context = context;
            _dbSet = _context.Set<TEntity>();
        }

        public IQueryable<TEntity> Get() => _dbSet;

        //public ResultList<TEntity> Get<TKey>(Expression<Func<TEntity, TKey>> order)
        //    => Get(order, null);

        public ResultList<TEntity> Get<TKey>(Expression<Func<TEntity, TKey>> order/*, IncludeList<TEntity> includes*/)
            => Get(x => true, order/*, includes*/);

        public ResultList<TEntity> Get<TKey>(Expression<Func<TEntity, bool>> filter, Expression<Func<TEntity, TKey>> order/*, IncludeList<TEntity> includes*/)
           => Get(filter, order, 1, int.MaxValue/*, includes*/);

        public ResultList<TEntity> Get<TKey>(Expression<Func<TEntity, bool>> filter, Expression<Func<TEntity, TKey>> order, int page, int itemsPerPage/*, IncludeList<TEntity> includes*/)
            => GetAsync(filter, order, page, itemsPerPage/*, includes*/).Result;

        public virtual async Task<ResultList<TEntity>> GetAsync<TKey>(
            Expression<Func<TEntity, bool>> filter,
            Expression<Func<TEntity, TKey>> order,
            int page,
            int itemsPerPage/*,
            IncludeList<TEntity> includes*/)
        {
            var skip = (page - 1) * itemsPerPage;
            var query = _dbSet.AsQueryable();

            /*if (includes != null)
                foreach (var item in includes)
                    query = query.Include(item);*/

            query = query.Where(filter);
            var total = await query.CountAsync();
            var result = await query
                .OrderBy(order)
                //.Skip(skip)
                //.Take(itemsPerPage)
                .ToListAsync();

            return new ResultList<TEntity>()
            {
                //Page = page,
                //ItemsPerPage = itemsPerPage,
                //TotalItems = total,
                Items = result
            };
        }
    }
}
