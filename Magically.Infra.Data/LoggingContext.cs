﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using JsonDiffPatchDotNet;
using Newtonsoft.Json;
using Magically.Domain.Common;

namespace Magically.Infra.Data
{
    public static class LoggingContext
    {
        private static readonly List<EntityState> entityStates = new List<EntityState>() { EntityState.Added, EntityState.Modified, EntityState.Deleted };

        public static async Task LogChanges(this ApplicationDbContext context)
        {
            var logTime = DateTime.Now;
            const string emptyJson = "{}";

            int user = 0;
            if (!string.IsNullOrEmpty(Thread.CurrentPrincipal?.Identity?.Name))
                user = Int32.Parse(Thread.CurrentPrincipal?.Identity?.Name);

            var changes = context.ChangeTracker.Entries()
                .Where(x => entityStates.Contains(x.State) && x.Entity.GetType().IsSubclassOf(typeof(BaseEntity)))
                .ToList();

            var jdp = new JsonDiffPatch();

            foreach (var item in changes)
            {
                var original = emptyJson;
                var updated = JsonConvert.SerializeObject(item.CurrentValues.Properties.ToDictionary(pn => pn.Name, pn => item.CurrentValues[pn]));
                var creationDate = DateTime.Now;

                if (item.State == EntityState.Modified)
                {
                    var dbValues = await item.GetDatabaseValuesAsync();
                    original = JsonConvert.SerializeObject(dbValues.Properties.ToDictionary(pn => pn.Name, pn => dbValues[pn]));

                    creationDate = dbValues.GetValue<DateTime>("CreationDate");
                }

                item.Property("CreationDate").CurrentValue = creationDate;

                string jsonDiff = jdp.Diff(original, updated);

                if (string.IsNullOrWhiteSpace(jsonDiff) == false)
                {
                    var EntityDiff = JToken.Parse(jsonDiff).ToString(Formatting.None);


                    var entry = context.Entry(item.Entity.GetType());
                    var primaryKey = entry.Metadata.FindPrimaryKey();
                    //var keys = primaryKey.Properties.ToDictionary(x => x.Name, x => x.PropertyInfo.GetValue(item.Entity.GetType()));                    
                    var logEntry = new Domain.Log()
                    {
                        EntityName = item.Entity.GetType().Name,
                        EntityID = item.CurrentValues[primaryKey.GetAnnotations().First().Value.ToString()].ToString(),
                        LogDateTime = logTime,
                        Operation = item.State.ToString(),
                        IDUsuario = user,
                        ValuesChanges = EntityDiff,
                    };

                    context.Logs.Add(logEntry);
                }

            }
        }
    }
}
