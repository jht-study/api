﻿using Magically.Domain;
using Magically.Infra.Data.Mapping;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Magically.Infra.Data
{
    public class ApplicationDbContext : DbContext
    {

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }
        public ApplicationDbContext() { }

        public DbSet<USR_Usuarios> Usuarios { get; set; }
        public DbSet<VisitaAcompanhada> VisitaAcompanhadas { get; set; }
        public DbSet<Metodo> Metodos { get; set; }
        public DbSet<Domain.Log> Logs { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            new LogMap(modelBuilder.Entity<Domain.Log>());
            new VisitaAcompanhadaMap(modelBuilder.Entity<VisitaAcompanhada>());
            new MetodoMap(modelBuilder.Entity<Metodo>());
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            await this.LogChanges();
            return await base.SaveChangesAsync(cancellationToken);
        }
    }
}
