﻿using Magically.Domain;
using Magically.Infra.Data.Repository.Interface;
using Magically.Service.Generic;
using Magically.Service.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Magically.Service.Concret
{
    public class MetodoService : BaseService<Metodo>, IMetodoService
    {
        private readonly IMetodoRepository _MetodoRepository;

        public MetodoService(IMetodoRepository metodoRepository) : base(metodoRepository)
        {
            _MetodoRepository = metodoRepository;
        }
    }
}
