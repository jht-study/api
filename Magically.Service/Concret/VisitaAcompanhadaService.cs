﻿using Magically.Domain;
using Magically.Infra.Data.Repository.Interface;
using Magically.Service.Generic;
using Magically.Service.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Magically.Service.Concret
{
    public class VisitaAcompanhadaService: BaseService<VisitaAcompanhada>, IVisitaAcompanhadaService
    {
        //private readonly IVisitaAcompanhadaRepository _visitaAcompanhadaRepository;
        //private readonly IVisitaAcompanhadaService _visitaAcompanhadaService;
        
        public VisitaAcompanhadaService(IVisitaAcompanhadaRepository visitaAcompanhadaRepository/*,
            IUnitOfWork unitOfWork,
            IValidator<User> validator,
            IVisitaAcompanhadaService visitaAcompanhadaService*/) : base(visitaAcompanhadaRepository/*, unitOfWork, validator*/)
        {
            //_visitaAcompanhadaRepository = visitaAcompanhadaRepository;
            //_visitaAcompanhadaService = visitaAcompanhadaService;
        }
    }
}
