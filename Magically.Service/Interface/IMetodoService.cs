﻿using Magically.Domain;
using Magically.Service.Generic;
using System;
using System.Collections.Generic;
using System.Text;

namespace Magically.Service.Interface
{
    public interface IMetodoService : IBaseService<Metodo>
    {
    }
}
