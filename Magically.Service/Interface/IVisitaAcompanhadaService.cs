﻿using Magically.Domain;
using Magically.Domain.Common;
using Magically.Service.Generic;

namespace Magically.Service.Interface
{
    public interface IVisitaAcompanhadaService : IBaseService<VisitaAcompanhada>
    {
        
    }
}
