﻿using Magically.Domain.Common;
using System;
using System.Linq.Expressions;

namespace Magically.Service.Generic
{
    public interface IBaseService<TEntity> where TEntity : class
    {
        /// <summary>
        /// Get ALL the entities, without filter, on the specified order, without child objects.
        /// <para>Use it at your own risk, as the number of data returned could be big.</para>
        /// <para>BE CAREFUL: could impact on the perfomance.</para>
        /// </summary>
        /// <returns>PageList with only 1 page and all the items</returns>
        ResultList<TEntity> Get<TKey>(Expression<Func<TEntity, TKey>> order);
    }
}
