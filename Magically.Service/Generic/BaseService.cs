﻿using Magically.Domain.Common;
using Magically.Infra.Data.Repository.Generic;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Magically.Service.Generic
{
    public class BaseService<TEntity> : IBaseService<TEntity> where TEntity : class
    {
        protected readonly IRepositoryGeneric<TEntity> _repository;
        //protected readonly IUnitOfWork _unitOfWork;
        //protected readonly IValidator<TEntity> _validator;

        public BaseService(IRepositoryGeneric<TEntity> repository/*, IUnitOfWork unitOfWork, IValidator<TEntity> validator*/)
        {
            _repository = repository;
            //_unitOfWork = unitOfWork;
            //_validator = validator;
        }

        public ResultList<TEntity> Get<TKey>(Expression<Func<TEntity, TKey>> order)
            => _repository.Get(order);
    }
}
